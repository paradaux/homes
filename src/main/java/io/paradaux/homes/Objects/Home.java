package io.paradaux.homes.Objects;

import org.bukkit.Location;

import java.util.List;
import java.util.UUID;

public class Home {

    UUID owner;
    String username;
    String name;
    List<UUID> members;
    Location location;

    public Home(UUID owner, String username, List<UUID> members, Location location) {
        this.owner = owner;
        this.username = username;
        this.members = members;
        this.location = location;
    }

    public UUID getOwner() {
        return owner;
    }

    public void setOwner(UUID owner) {
        this.owner = owner;
    }

    public String getUsername() {
        return username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<UUID> getMembers() {
        return members;
    }

    public void setMembers(List<UUID> members) {
        this.members = members;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }


}
