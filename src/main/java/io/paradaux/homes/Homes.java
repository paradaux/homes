package io.paradaux.homes;

import io.paradaux.homes.API.*;
import io.paradaux.homes.Commands.*;
import ninja.egg82.sql.SQL;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.util.HashMap;
import java.util.UUID;

public final class Homes extends JavaPlugin {

    private static SQL sql;
    public static SQL getSQL() { return sql; }

    private static ConfigurationCache ConfigurationCache;
    public static ConfigurationCache getConfigurationCache() { return ConfigurationCache; }

    private static HashMap<Player, Long> cooldown = new HashMap<>();
    public static HashMap<Player, Long> getCooldown() { return cooldown; }

    private static HashMap<UUID, Integer> pendingTeleports = new HashMap<>();
    public static HashMap<UUID, Integer> getPendingTeleports = new HashMap<>();

    private static HomeManager homeManager = new HomeManager();
    public static HomeManager getHomeManager() { return homeManager; }

    private static File localeFile;
    private static YamlConfiguration locale;

    public static YamlConfiguration getLocale() { return locale; }
    public static File getLocaleFile() { return localeFile; }

    public static JobManager jobManager = new JobManager();
    public static JobManager getJobManager() { return jobManager; }


    @Override
    public void onEnable() {
        // Startup Messages
        System.out.println("\n" +
                "+ ------------------------------------ +\n" +
                "|        Running Homes v1.0.0          |\n" +
                "|       © Rían Errity (Paradaux)       |\n" +
                "|         https://paradaux.io          |\n" +
                "+ ------------------------------------ +\n"
        );

        getLogger().info("This plugin was developed by Rían Errity (Paradaux) for Acramus");
        getLogger().info("Homes: ${project.version} is initialising.");

        this.getConfig().options().copyDefaults();
        saveDefaultConfig();
        saveResource("locale.yml", false);

        registerCommands(this);

        ConfigurationCache.initialiseConfigurationValues(this.getConfig());

        sql = MySQL.createSQL();
        locale = LocaleManager.initiateDataFile();
    }

    @Override
    public void onDisable() {
        sql.close();
        getLogger().info("Homes has been disabled.");
    }

    @SuppressWarnings("ConstantConditions")
    public void registerCommands(JavaPlugin p) {
        p.getCommand("homes").setExecutor(new homesCMD());
        p.getCommand("sethome").setExecutor(new setHomeCMD());
        p.getCommand("delhome").setExecutor(new delHomeCMD());
        p.getCommand("addtohome").setExecutor(new addToHomeCMD());
        p.getCommand("removefromhome").setExecutor(new removeFromHomeCMD());
        p.getCommand("homemembers").setExecutor(new homeMembersCMD());
        p.getCommand("homesadmin").setExecutor(new homesAdminCMD());
    }
}
