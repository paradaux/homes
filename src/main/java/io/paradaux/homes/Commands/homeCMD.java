package io.paradaux.homes.Commands;

import io.paradaux.homes.API.HomeManager;
import io.paradaux.homes.Homes;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashMap;

public class homeCMD implements CommandExecutor {

    Player player;
    HashMap<Player, Long> cooldown = Homes.getCooldown();
    HomeManager homeManager = Homes.getHomeManager();


    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            System.out.println("Error: Console may not use commands pertaining to Homes/");
            return true;
        }
        player = (Player) sender;


        // If Player can use /home
        if (homeManager.isOnCooldown(player) || homeManager.isCooldownExpired(player)) {
            homeManager.cooldownPlayer(player);


            // get user's homes

            // get Home with specified name

            // teleport to home

            // remove home from memory.

        // If the user is timed out
        } else {

            long timeRemainingLong = (Homes.getCooldown().get(player) - System.currentTimeMillis());
            int timeRemaining = (int) (timeRemainingLong/1000);
            sender.sendMessage("You must wait " + timeRemaining + " seconds before you can do that again.");
        }
        return false;
    }
    
}
