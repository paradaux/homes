package io.paradaux.homes.Events;

import io.paradaux.homes.Homes;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import java.util.HashMap;
import java.util.UUID;

public class PlayerMoveEventListener implements Listener {

    HashMap<UUID, Integer> pendingTeleports = Homes.getPendingTeleports;

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent e) {
        Player p = e.getPlayer();
        if (pendingTeleports.containsKey(p)) {
            Bukkit.getScheduler().cancelTask(pendingTeleports.get(p));
            pendingTeleports.remove(p);
            p.sendMessage("Your teleport was interrupted.");
        }


    }
}
