package io.paradaux.homes.API;

import io.paradaux.homes.Homes;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;

public class LocaleManager {
    private static File localeFile;

    public static YamlConfiguration initiateDataFile() {
        return YamlConfiguration.loadConfiguration(Homes.getLocaleFile());
    }

}
