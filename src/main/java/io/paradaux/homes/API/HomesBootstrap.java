package io.paradaux.homes.API;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URLClassLoader;

public class HomesBootstrap extends JavaPlugin {

    // Coming soon: Dependancy Injection.
    private final Logger logger = LoggerFactory.getLogger(getClass());

    private URLClassLoader proxiedClassLoader;

    private Object concrete;
    private Class<?> concreteClass;

    private final boolean isBukkit;

    public HomesBootstrap() {
        super();
        isBukkit = Bukkit.getName().equals("Bukkit") || Bukkit.getName().equals("CraftBukkit");
    }


//    @Override
//    public void onLoad() {
//        proxiedClassLoader = new ProxiedURLClassLoader(getClass().getClassLoader());
//
//        try {
//            concreteClass = proxiedClassLoader.loadClass("co.paradaux.hdiscord.Main");
//            concrete = concreteClass.getDeclaredConstructor(Plugin.class).newInstance(this);
//            concreteClass.getMethod("onLoad").invoke(concrete);
//        } catch (ClassNotFoundException | NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException ex) {
//            logger.error(ex.getMessage(), ex);
//            throw new RuntimeException("Could not create main class.");
//        }
//    }

}
