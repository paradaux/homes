package io.paradaux.homes.API;

import io.paradaux.homes.Homes;
import org.bukkit.entity.Player;

import java.util.HashMap;

public class HomeManager {

    HashMap<Player, Long> cooldown = Homes.getCooldown();

    public HomeManager() {}

    public boolean isOnCooldown(Player player) {
        return cooldown.containsKey(player);
    }

    public boolean isCooldownExpired(Player player) {
        boolean isExpired = cooldown.get(player) > System.currentTimeMillis();
        if (isExpired) { cooldown.remove(player); }
        return isExpired;
    }

    public void cooldownPlayer(Player player) {
        cooldown.put(player, System.currentTimeMillis() + (Homes.getConfigurationCache().getTeleportationCooldown() * 1000));
    }

    public int timeRemaining(Player player) {
        long timeRemainingLong = (Homes.getCooldown().get(player) - System.currentTimeMillis());
        return (int) (timeRemainingLong/1000);
    }
}
