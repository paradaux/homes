package io.paradaux.homes.API;

import com.zaxxer.hikari.HikariConfig;
import io.paradaux.homes.Homes;
import ninja.egg82.core.SQLQueryResult;
import ninja.egg82.sql.SQL;
import org.bukkit.plugin.Plugin;

import java.sql.SQLException;

public class MySQL {

    public static void createTables(Plugin p) throws SQLException {
        SQL sql = Homes.getSQL();
        String tablePrefix = Homes.getConfigurationCache().getDbPrefix();

        if (!tableExists(tablePrefix + "homes", p)) {
            p.getLogger().info("First startup detected. creating tables.");
            /*  Insert SQL Query to create tables here */
            sql.execute("");

        } else {
            p.getLogger().info("Thank you for using Homes by Paradaux.");
        }
    }

    private static boolean tableExists(String tableName, Plugin p) throws SQLException {
        SQL sql = Homes.getSQL();
        String databaseName = Homes.getConfigurationCache().getDbName();

        SQLQueryResult query = sql.query("SELECT COUNT(*) FROM information_schema.tables WHERE table_schema=? AND table_name=?;", databaseName, tableName);
        return query.getData().length > 0 && query.getData()[0].length > 0 && ((Number) query.getData()[0][0]).intValue() != 0;
    }

    public static SQL createSQL() {
        final String address = Homes.getConfigurationCache().getDbAddress();
        final String dbname = Homes.getConfigurationCache().getDbName();
        final String dbuser = Homes.getConfigurationCache().getDbUser();
        final String dbpw = Homes.getConfigurationCache().getDbPass();

        final HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setJdbcUrl("jdbc:mysql://" + address + "/" +  dbname);
        hikariConfig.setConnectionTestQuery("SELECT 1;");
        hikariConfig.setUsername(dbuser);
        hikariConfig.setPassword(dbpw);
        hikariConfig.setMaximumPoolSize(2);
        hikariConfig.setMinimumIdle(2);
        hikariConfig.setMaxLifetime(1800000L);
        hikariConfig.setConnectionTimeout(5000L);
        hikariConfig.addDataSourceProperty("useUnicode", true);
        hikariConfig.addDataSourceProperty("characterEncoding", "utf8");
        hikariConfig.addDataSourceProperty("useLegacyDatetimeCode", false);
        hikariConfig.addDataSourceProperty("serverTimezone", "UTC");
        hikariConfig.setAutoCommit(true);
        hikariConfig.addDataSourceProperty("useSSL", false);
        hikariConfig.addDataSourceProperty("cacheServerConfiguration", "true");
        hikariConfig.addDataSourceProperty("useLocalSessionState", "true");
        hikariConfig.addDataSourceProperty("useLocalTransactionState", "true");
        hikariConfig.addDataSourceProperty("rewriteBatchedStatements", "true");
        hikariConfig.addDataSourceProperty("useServerPrepStmts", "true");
        hikariConfig.addDataSourceProperty("cachePrepStmts", "true");
        hikariConfig.addDataSourceProperty("maintainTimeStats", "false");
        hikariConfig.addDataSourceProperty("useUnbufferedIO", "false");
        hikariConfig.addDataSourceProperty("useReadAheadInput", "false");
        hikariConfig.addDataSourceProperty("prepStmtCacheSize", "250");
        hikariConfig.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
        hikariConfig.addDataSourceProperty("cacheResultSetMetadata", "true");
        hikariConfig.addDataSourceProperty("elideSetAutoCommits", "true");

        return new SQL(hikariConfig);

    }

}
