package io.paradaux.homes.API;

import org.bukkit.configuration.file.FileConfiguration;

public class ConfigurationCache {

    private double configurationVersion;
    private int teleportationCooldown;
    private int homeAllowance;

    private String dbAddress;
    private String dbPort;
    private String dbName;
    private String dbPrefix;
    private String dbUser;
    private String dbPass;

    public ConfigurationCache(double configurationVersion, int teleportationCooldown, int homeAllowance, String dbAddress, String dbPort, String dbName, String dbPrefix, String dbUser, String dbPass) {
        this.configurationVersion = configurationVersion;
        this.teleportationCooldown = teleportationCooldown;
        this.homeAllowance = homeAllowance;
        this.dbAddress = dbAddress;
        this.dbPort = dbPort;
        this.dbName = dbName;
        this.dbPrefix = dbPrefix;
        this.dbUser = dbUser;
        this.dbPass = dbPass;
    }

    public void initialiseConfigurationValues(FileConfiguration config) {
        setConfigurationVersion(config.getDouble("config-version"));
        setTeleportationCooldown(config.getInt("teleport-cooldown"));
        setHomeAllowance(config.getInt("home-allowance"));
        setDbAddress(config.getString("database-connection.address"));
        setDbPort(config.getString("database-connection.port"));
        setDbName(config.getString("database-connection.database-name"));
        setDbPrefix(config.getString("database-connection.table-prefix"));
        setDbUser(config.getString("database-connection.db-user"));
        setDbPass(config.getString("database-connection.db-pass"));
    }

    public double getConfigurationVersion() {
        return configurationVersion;
    }

    public void setConfigurationVersion(double configurationVersion) {
        this.configurationVersion = configurationVersion;
    }

    public int getTeleportationCooldown() {
        return teleportationCooldown;
    }

    public void setTeleportationCooldown(int teleportationCooldown) {
        this.teleportationCooldown = teleportationCooldown;
    }

    public int getHomeAllowance() {
        return homeAllowance;
    }

    public void setHomeAllowance(int homeAllowance) {
        this.homeAllowance = homeAllowance;
    }

    public String getDbAddress() {
        return dbAddress;
    }

    public void setDbAddress(String dbAddress) {
        this.dbAddress = dbAddress;
    }

    public String getDbPort() {
        return dbPort;
    }

    public void setDbPort(String dbPort) {
        this.dbPort = dbPort;
    }

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public String getDbPrefix() {
        return dbPrefix;
    }

    public void setDbPrefix(String dbPrefix) {
        this.dbPrefix = dbPrefix;
    }

    public String getDbUser() {
        return dbUser;
    }

    public void setDbUser(String dbUser) {
        this.dbUser = dbUser;
    }

    public String getDbPass() {
        return dbPass;
    }

    public void setDbPass(String dbPass) {
        this.dbPass = dbPass;
    }
}
