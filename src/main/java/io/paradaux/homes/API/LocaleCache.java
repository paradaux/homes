package io.paradaux.homes.API;

import io.paradaux.homes.Homes;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;

import java.util.List;

public class LocaleCache {
    YamlConfiguration locale = Homes.getLocale();

    double localeversion;
    String chatprefix;

    String addtohome_usage;
    String delhome_usage;
    String homeMembers_usage;
    String homes_format;
    String removefromhome_usage;
    String sethome_usage;

    String home_helpheader;
    String home_helpcontent;

    String homesadmin_helpheader;
    String homesadmin_helpcontent;

    private String colorise(String query) {
        return ChatColor.translateAlternateColorCodes('&', query);
    }

    private String getFromPath(String path) {
        return colorise(Homes.getLocale().getString(path));
    }

    private double getDoubleFromPath(String path) {
        return Homes.getLocale().getDouble(path);
    }
    private List<String> getListFromPath(String path) {
        return Homes.getLocale().getStringList(path);
    }

    private String concatenateString(List<String> args) {
        return colorise(String.join("\n", args));
    }

    public LocaleCache(double localeversion, String chatprefix, String addtohome_usage, String delhome_usage, String homeMembers_usage, String homes_format, String removefromhome_usage, String sethome_usage, String home_helpheader, String home_helpcontent, String homesadmin_helpheader, String homesadmin_helpcontent) {
        this.localeversion = getDoubleFromPath("locale_version");
        this.chatprefix = getFromPath("chat_prefix");
        this.addtohome_usage = getFromPath("addToHome.noargs_usage");
        this.delhome_usage = getFromPath("delHome.noargs_usage");
        this.homeMembers_usage = getFromPath("homeMembers.noargs_usage");
        this.homes_format = getFromPath("homes.homelist_format");
        this.removefromhome_usage = getFromPath("removeFromHome.noargs_usage");
        this.sethome_usage = getFromPath("setHome.noargs_usage");
        this.home_helpheader = getFromPath("home.help_header");
        this.home_helpcontent = concatenateString(getListFromPath("hom.help_content"));
        this.homesadmin_helpheader = getFromPath("homesAdmin.help_header");
        this.homesadmin_helpcontent = concatenateString(getListFromPath("homeAdmin.help_content"));
    }

    public YamlConfiguration getLocale() {
        return locale;
    }

    public double getLocaleversion() {
        return localeversion;
    }

    public String getChatprefix() {
        return chatprefix;
    }

    public String getAddtohome_usage() {
        return addtohome_usage;
    }

    public String getDelhome_usage() {
        return delhome_usage;
    }

    public String getHomeMembers_usage() {
        return homeMembers_usage;
    }

    public String getHomes_format() {
        return homes_format;
    }

    public String getRemovefromhome_usage() {
        return removefromhome_usage;
    }

    public String getSethome_usage() {
        return sethome_usage;
    }

    public String getHome_helpheader() {
        return home_helpheader;
    }

    public String getHome_helpcontent() {
        return home_helpcontent;
    }

    public String getHomesadmin_helpheader() {
        return homesadmin_helpheader;
    }

    public String getHomesadmin_helpcontent() {
        return homesadmin_helpcontent;
    }
}
