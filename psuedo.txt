==== Commands ====
? denotes optional parameter.

1. :: /home <name> <?user> -- Teleports to <name> home, if <user> is specified, teleport to that player's home with the given name.
1. :: /sethome <name> -- Sets your current position as a home
2. :: /delhome <name> -- Deletes a named home.
3. :: /addtohome <online username> <name> -- Grants <online username> access to home <name>
4. :: /removefromhome <online username> <name> -- Removes <online username>'s access to home <name>
5. :: /homemembers <name> -- lists members who have access to home <name>
6, :: /homes <?help> -- Either lists homes or gives the user a help menu

== Home Admin Commands ==

1. :: /homesadmin teleportto <online username> <home name>
2. :: /homesadmin remove <online us ername> <home name>

==== Permissions ====

Permissions
homes.use
homes.set
homes.del
homes.add
homes.remove
homes.members
homes.bypass.cooldown
homes.bypass.access
homes.homes

== Admin Permissions ==
homes.admin
homes.admin.teleportto
homes.admin.remove

==== Configuration Values ====

config-version:
mysql-database:
    address:
    port:
    dbname:
    user:
    pass:

default-home-allowance: 4
cooldown: 10

==== Schema ====

Table: homes_homes

UUID, Username, Name, Location, List